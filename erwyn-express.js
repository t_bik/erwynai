/*
 *	This file is part of ErwynAI.
 *
 *  ErwynAI is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   ErwynAI is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

const express = require('express');
const fs = require('fs');
const https = require('https');
const http = require('http');
const path = require('path');
const ip = require('ip');
const { exec } = require('child_process');

const { erwynSocket } = require('./libs/erwyn-socket');

const SSLOPTIONS = {
	ca: fs.readFileSync('ssl/ca.crt', 'utf-8'),
	key: fs.readFileSync('ssl/server.key', 'utf-8'),
	cert: fs.readFileSync('ssl/server.crt', 'utf-8')
}

const privateKey = fs.readFileSync('ssl/server.key', 'utf-8');
const certificate = fs.readFileSync('ssl/server.crt', 'utf-8');

const app = express();
const PORT_SSL = process.env.HTTPS_PORT || 3001;
const PORT = process.env.PORT || 3000;

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', (req, res) => { res.render('main'); });

const httpsServer = https.createServer(SSLOPTIONS, app);
const httpServer = http.createServer(app);

httpsServer.listen(PORT_SSL, () => {
	if (process.platform === 'win32') {
		process.stdout.write('\033c');
	} else {
		process.stdout.write('\033');
	}
	console.log(fs.readFileSync('motd', 'utf-8'));
	console.log(`Erwyn AI è online!\n\nURL_HTTPS: https://${ip.address()}:${PORT_SSL}`);
	httpServer.listen(PORT, () => {
		console.log(`URL_HTTP: http://${ip.address()}:${PORT}\n`);
	})
});



erwynSocket(httpsServer, httpServer);