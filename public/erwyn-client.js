/*
 *	This file is part of ErwynAI.
 *
 *  ErwynAI is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   ErwynAI is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

const socket = io();

const resetHTMLText = (id) => {
	$(id).text('');
};

socket.on('getData', (data) => {
	$('#welcome-text').text(data.data);
	$('#voice').text(data.voice);
	resetHTMLText('#voiceTR');

	let erwynVoice = new SpeechSynthetisUtterance('test');
	erwynVoice.lang = 'it-IT';
	window.speechSynthetis.speak(erwynVoice);
})

$('#tap-to-speech').on('click', () => {
	if('webkitSpeechRecognition' in window) {
		let recognition = new webkitSpeechRecognition();
		recognition.continuous = true;
		recognition.interimResults = true;

		recognition.lang = 'it-IT';
		recognition.start();

		let voiceTranscript = '';

		recognition.onresult = (e) => {
			let speechToText = '';
			let test = '';
			resetHTMLText('#voice');

			for (let i = e.resultIndex; i < e.results.length; ++i) {
				if (e.results[i].isFinal) {
					speechToText += e.results[i][0].transcript;
				} 
				$('#voiceTR').text(speechToText);
			} 
			voiceTranscript = speechToText;
		}

		recognition.onspeechend = () => {
			recognition.stop();
			socket.emit('receiveData', voiceTranscript);

			voiceTranscript = '';
		}
	} else {
		alert('Il browser non è supportato, usa Chrome o un qualsiasi browser WebKit');
	}
});

