/*
 *	This file is part of ErwynAI.
 *
 *  ErwynAI is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   ErwynAI is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

const fs = require('fs');
const lame = require('lame');
const Speaker = require('speaker');

let vol;

if (process.platform === 'win32') {
	vol = require('./vol/vol');
} else {
	vol = require('./vol/linux-vol');
}

let readStream = undefined;
let volume = 0.3;

const volumeAudio = (diff) => {
	if (readStream !== undefined) {
		volume += diff;

		if (volume >= 1) { volume = 1 }
		else if (volume <= 0) { volume = 0 };

		vol.set(volume);
		return `Volume modificato`;
	}
};

const stopAudio = () => {
	if (readStream !== undefined) {
		readStream.emit('close');
		readStream = undefined;

		console.log('=> Riproduzione interrotta');
		return 'Riproduzione interrotta';
	}
};

const playAudio = (audio) => {
	return new Promise((resolve, reject) => {
		if (readStream !== undefined) { return; }

		decoder = fs.createReadStream(audio);
		vol.set(volume);

		decoder
			.pipe(new lame.Decoder())
			.on('format', function (format) {
				readStream = this.pipe(new Speaker(format))
					.on('finish', () => { resolve(); })
			});
	});
};

module.exports = { playAudio, stopAudio, volumeAudio };