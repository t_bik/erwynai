/*
 *	This file is part of ErwynAI.
 *
 *  ErwynAI is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   ErwynAI is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

const fs = require('fs');
const awaitEach = require('await-each');
const { playAudio, stopAudio, volumeAudio } = require('./erwyn-audio');

module.exports = class erwynAI {

	static setPlaylist(folder) {
		return new Promise((resolve, reject) => {
			let filesArray = [];
			try {
				fs.readdir(`./resources/${folder.toString()}`, (err, files) => {
					if (err !== null) { reject(undefined); return; }
					files.forEach((file) => {
						filesArray.push(`./resources/${folder.toString()}/${file}`);
					});
					resolve(filesArray);
				});
			} catch(e) { reject(e); }
		});
	}

	static ferma(data) {
		if (data.includes('musica') || data.includes('riproduzione')) {
			return stopAudio();
		}
	}

	static alza(data) {
		if (data.includes('volume')) {
			if (data.includes('massimo')) {
				return volumeAudio(1);
			}
			volumeAudio(0.2);
		}
	}

	static riduci(data) {
		if (data.includes('volume')) {
			 if (data.includes('minimo')) {
				return volumeAudio(-1);
			}
			volumeAudio(-0.2);
		}
	}

	static riproduci(data) {
		this.setPlaylist(data)
			.then((files) => {
				if (files === undefined) {
					return;
				}
				awaitEach(files, async (file) => {
					console.log(`=> Riproduco ${file}`);
					await playAudio(file)
						.then((res) => { 
							return; 
						});
				});
			}).catch((e) => console.log(e));
		return `Riproduco ${data}`;
	}
};