/*
 *	This file is part of ErwynAI.
 *
 *  ErwynAI is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *   ErwynAI is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
 */

const socket = require('socket.io');

const erwynAI  = require('./erwyn-ai');

const erwynSocket = (httpsServer, httpServer) => {
	const io = new socket();

	io.attach(httpsServer);
	io.attach(httpServer);

	io.on('connection', (sc) => {
		console.log('Nuovo device connesso a Erwyn AI!\n');

		sc.on('receiveData', (data) => {
			const voiceCommand = data.split(' ')[0];
			const voiceVar = data.split(' ').slice(1).join(' ');
			let dataEmit = '';

			try {
				dataEmit = erwynAI[voiceCommand](voiceVar);
			} catch(e) {
				if (e.name === 'TypeError') {
					dataEmit = 'Comando non riconosciuto';
				} else { dataEmit = `Errore: ${e.name}` }
			}

			sc.emit('getData', {voice: data, data: dataEmit});
		});
	});
};

module.exports =  { erwynSocket };